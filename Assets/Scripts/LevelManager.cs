﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public string nextScene;

    public void LoadLevel() {
        SceneManager.LoadScene(nextScene);
    }
}