﻿using UnityEngine;

public class EnnemyController : MonoBehaviour
{
    public Vector2 VelocityVector;
    public float SpeedDifficultyFactor = .3f;
    public float DifficultyTimeout = 10f;
    public LevelManager levelManager;

    private float difficultyStartTime;

    private void Start()
    {
        difficultyStartTime = Time.time;
    }

    private void Update()
    {
        if (Time.time - difficultyStartTime > DifficultyTimeout)
        {
            VelocityVector =new Vector2(
                VelocityVector.x * SpeedDifficultyFactor,
                VelocityVector.y * SpeedDifficultyFactor
                );
            difficultyStartTime = Time.time;
        }
        transform.position = new Vector3(
            transform.position.x+VelocityVector.x*Time.deltaTime,
            transform.position.y+VelocityVector.y * Time.deltaTime,
            transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Wall")) {
            // rebound
            if (collision.name.Equals("Wall_Left")
                || collision.name.Equals("Wall_Right"))
            {
                VelocityVector.x = -VelocityVector.x;
            }
            else {
                VelocityVector.y = -VelocityVector.y;
            }
        }
        else if (collision.tag.Equals("Player"))
        {
            levelManager.nextScene = "Menu";
            levelManager.LoadLevel();
        }
    }
}